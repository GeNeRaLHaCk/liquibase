package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.PersonService;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dao.PersonMapper;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.dto.PersonDTO;
import com.example.exampleliqubase.model.EmploymentEntity;
import com.example.exampleliqubase.model.PersonEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DataSource dataSource;
    @Override
    @Transactional
    public PersonDTO save(PersonDTO personDTO) {

        PersonEntity personEntity = modelMapper.map(personDTO, PersonEntity.class);
        //тут какая-то логика

        personMapper.save(personEntity);
        //тут еще какая-то логика

        return modelMapper.map(personEntity, PersonDTO.class);
    }

    @Override
    public List<PersonDTO> getListPersons() {
        return null;
    }
}
