package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dao.AccountMapper;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dto.AccountDTO;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.AccountEntity;
import com.example.exampleliqubase.model.EmploymentEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Service
public class EmploymentServiceImpl implements EmploymentService {

    @Autowired
    private EmploymentMapper employmentMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DataSource dataSource;
    @Override
    @Transactional
    public EmploymentDTO save(EmploymentDTO employmentDTO) {

        EmploymentEntity employmentEntity = modelMapper.map(employmentDTO, EmploymentEntity.class);
        //тут какая-то логика

        employmentMapper.save(employmentEntity);
        //тут еще какая-то логика

        return modelMapper.map(employmentEntity, EmploymentDTO.class);
    }

    @Override
    public List<EmploymentDTO> getListEmployees() {
        return null;
    }
}
