package com.example.exampleliqubase.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmploymentEntity {
    private Long employment_id;
    private int version;
    private Date start_dt;
    private Date end_dt;
    private Long work_type_id;
    private String organization_name;
    private String position_name;
    private int person_id;
}
