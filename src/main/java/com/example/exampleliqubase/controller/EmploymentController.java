package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.AccountService;
import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dto.AccountDTO;
import com.example.exampleliqubase.dto.EmploymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/employment")
public class EmploymentController {
    @Autowired
    private EmploymentService employmentService;

    @PostMapping("/api/employments/update/{personId}")
    public EmploymentDTO saveAccount(@RequestBody @Validated EmploymentDTO employmentDTO) {
        return employmentService.save(employmentDTO);
    }

    @PostMapping("/list")
    public List<EmploymentDTO> getListAccounts() {

        return employmentService.getListEmployees();
    }

}
