package com.example.exampleliqubase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonDTO {
    @NotNull
    private Long person_id;
    @NotNull
    private String first_name;
    @NotNull
    private String last_name;
    @NotNull
    private String middle_name;
    private Date birth_date;
    @NotNull
    private String gender;
}
