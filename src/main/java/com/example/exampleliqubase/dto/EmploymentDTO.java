package com.example.exampleliqubase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentDTO {
    @NotNull
    private Long employment_id;
    @NotNull
    private int version;
    private Date start_dt;
    private Date end_dt;
    private Long work_type_id;
    private String organization_name;
    private String position_name;
    private int person_id;
}
