package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.dto.PersonDTO;

import java.util.List;

public interface PersonService {
    PersonDTO save(PersonDTO personDTO);

    List<PersonDTO> getListPersons();

}
