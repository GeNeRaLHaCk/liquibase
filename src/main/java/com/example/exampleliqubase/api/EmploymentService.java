package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.AccountDTO;
import com.example.exampleliqubase.dto.EmploymentDTO;

import java.util.List;

public interface EmploymentService {
    EmploymentDTO save(EmploymentDTO employmentDTO);

    List<EmploymentDTO> getListEmployees();

}
