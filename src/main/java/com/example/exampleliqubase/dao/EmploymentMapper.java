package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.AccountEntity;
import com.example.exampleliqubase.model.EmploymentEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface EmploymentMapper {
    @SelectKey(resultType = Long.class, keyProperty = "userId", before = true,
            statement = "select nextval('employment_employment_id_seq')")
    @Insert("insert into employment (user_id, username, password, country) " +
            "values (#{userId}, #{username} ,#{password} ,#{country} )")
    void save(EmploymentEntity employmentEntity);

    @Select("select * from employment")
    List<EmploymentEntity> getListEmployees();
}
