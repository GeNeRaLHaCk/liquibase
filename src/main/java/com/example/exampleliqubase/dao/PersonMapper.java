package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.EmploymentEntity;
import com.example.exampleliqubase.model.PersonEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PersonMapper {
    @SelectKey(resultType = Long.class, keyProperty = "userId", before = true,
            statement = "select nextval('person_person_id_seq')")
    @Insert("insert into person (person_id, first_name, last_name, middle_name) " +
            "values (#{personId}, #{username} ,#{password} ,#{country} )")
    void save(PersonEntity personEntity);

    @Select("select * from employment")
    List<PersonEntity> getListPersons();
}
